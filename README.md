# `ink-api` Deployment Config

This repository contains a base image for [ink-api](https://gitlab.coko.foundation/INK/ink-api) and the templates used to deploy to Kubernetes.

## Templates

See [pubsweet/infra](https://gitlab.coko.foundation/pubsweet/infra) and [pubsweet/deployer](https://gitlab.coko.foundation/pubsweet/deployer) for more details about how these templates work.

## Images

The base image for ink-api installs gems for the current `ink-api` repository in order to speed up builds there. It can be rebuilt and pushed to docker hub by triggering the relevant job in this repository. The image for `ink-api` itself extends this base image.

